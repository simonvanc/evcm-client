package com.example.testclientevcm;

import android.content.DialogInterface;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/*
Classe pour le Fragment/Tabs des prestations
 */
public class PrestaFragment extends Fragment {

    View rootView;
    ExpandableListView lv;
    private  int[] flag;
    private String[] groups;
    private String[][] children;


    public PrestaFragment() {

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        flag = new int[] {R.drawable.help, R.drawable.info, R.drawable.install};
        groups =  new String[] {"Dépannage", "Conseil/ Formation", "Installation"};
        children = new String [][] {
                { "Un incident paralyse votre activité. Ce n’est pas la première fois. Après un diagnostic précis, nous expliquons l’origine du problème et trouvons une solution. Soyez rassurés avec nos conseils préventifs pour éviter toute rechute." },
                { "Gagnez du temps avec des conseils et des astuces adaptés à votre activité. Les Helpers vous aident à prendre en main vos outils numériques ou approfondir l’utilisation de vos logiciels avec une approche didactique." },
                { "Vous possédez ou venez d’acquérir un appareil numérique. Libérez-vous des barrières techniques et des livres d’installation. Gardez l’esprit tranquille en nous confiant vos paramètrages et une assistance sur mesure." },};

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_presta, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lv = (ExpandableListView) view.findViewById(R.id.expListView);
        lv.setAdapter(new ExpandableListAdapter(groups, children, flag));
        lv.setGroupIndicator(null);

    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private final LayoutInflater inf;
        private String[] groups;
        private String[][] children;
        private int[] flag;


        public ExpandableListAdapter(String[] groups, String[][] children, int[] flag) {
            this.groups = groups;
            this.flag = flag;
            this.children = children;
            inf = LayoutInflater.from(getActivity());
        }

        @Override
        public int getGroupCount() {
            return (groups.length);
        }
        @Override
        public int getChildrenCount(int groupPosition) {
            return children[groupPosition].length;
        }



        @Override
        public Object getGroup(int groupPosition) {
            return groups [groupPosition];
        }
        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return children[groupPosition][childPosition];
        }



        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }
        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }



        @Override
        public boolean hasStableIds() {
            return true;
        }



        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {
                convertView = inf.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.lblListItem);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.text.setText(getChild(groupPosition, childPosition).toString());
            return convertView;
        }
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = inf.inflate(R.layout.list_group, parent, false);
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.lblListHeader);
                //holder.image = (ImageView) convertView.findViewById(R.id.presta_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.text.setText(getGroup(groupPosition).toString());
            return convertView;
        }



        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }



        private class ViewHolder {
            TextView text;
            ImageView image;
        }


    }
}

