package com.example.testclientevcm;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;

/**
 * Created by Simon on 28/05/2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private String[] mDataset;
    private String[] mDt;
    private int[] flag;


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public CardView mCardView;
        public TextView mTextView;
        public TextView mTV;
        public ImageView mIV;
        public MyViewHolder(View v){
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view);
            mTextView = (TextView) v.findViewById(R.id.tv_text);
            mTV = (TextView) v.findViewById(R.id.tv_blah);
           mIV = (ImageView) v.findViewById(R.id.materiel_image);
        }

    }


    //public MyAdapter(String[] myDataset, String[] myDt, Drawable myDraw){
    public MyAdapter(String[] myDataset, String[] myDt, int[] myDiv){
        mDataset = myDataset;
        mDt = myDt;
        flag = myDiv;

    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        holder.mTextView.setText(mDataset[position]);
        holder.mTV.setText(mDt[position]);
        holder.mIV.setImageResource(flag[position]);
        //holder.mIV.setInte
        //holder.mIV.setImageDrawable(mDiv);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

}
