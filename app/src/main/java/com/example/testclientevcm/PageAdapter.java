package com.example.testclientevcm;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Simon on 22/05/2018.
 */

public class PageAdapter extends FragmentPagerAdapter {

    //taille des Android Tablayout
    private int numOfTabs;

    //MAJ du constructeur qui inclu la variable
    PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }


    @Override
    //GETITEM: là où l'on va initialiser le fragment for AndroidTablayout
    public Fragment getItem(int position) {
        //On défini les fragments
        switch (position) {
            case 0:
                return new MaterielFragment();
            case 1:
                return new PrestaFragment();
            case 2:
                return new FormFragment();
            default:
                return null;
        }    }

    @Override
    //GETCOUNT: retourne le nb de tabs qui apparaissent dans le Android Tablayout
    public int getCount() {
        //On retourne le nombre de Tabs
        return numOfTabs;
    }
}
