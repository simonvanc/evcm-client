package com.example.testclientevcm;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


//Fragment
public class MaterielFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_materiel, container,false);
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);

        rv.setHasFixedSize(true);

        MyAdapter adapter = new MyAdapter(new String[]{"Ordinateur", "Smartphone", "Périphérique", "Box Internet", "Multimedia" , "Domotique"},
                new String[]{"PC/MAC", "Android, IPhone, Windows Phone", "Imprimante, écran", "Réseau", "Enceintes", "Objet connecté" },
                new int[]{R.drawable.laptop, R.drawable.smartphone, R.drawable.router, R.drawable.box, R.drawable.multimedia, R.drawable.house});
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        //return inflater.inflate(R.layout.fragment_materiel, container, false);

        return rootView;
    }



    /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chats, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_chat) {
            Toast.makeText(getActivity(), "Clicked on " + item.getTitle(), Toast.LENGTH_SHORT)
                    .show();
        }
        return true;
    }*/

}