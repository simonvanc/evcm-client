package com.example.testclientevcm;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Simon on 28/05/2018.
 */

public class MyPrestAdapter extends RecyclerView.Adapter<MyPrestAdapter.MyViewHolder> {

    private String[] mDataset;
    private int[] flag;




    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public ExpandableListView mExpandableListView;
        public TextView mTextView;
        public ImageView mIV;


        public MyViewHolder(View v){
            super(v);

            mIV = (ImageView) v.findViewById(R.id.materiel_image);
            mExpandableListView = (ExpandableListView) v.findViewById(R.id.expListView);
            mTextView = (TextView) v.findViewById(R.id.lblListItem);
        }

    }


    public MyPrestAdapter(String[] myDataset, int[] myDiv){
        mDataset = myDataset;
        flag = myDiv;

    }

    @Override
    public MyPrestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_group, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        holder.mTextView.setText(mDataset[position]);
        holder.mIV.setImageResource(flag[position]);

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

}
